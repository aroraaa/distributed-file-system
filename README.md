# DistributedFileSystem
---
**Aashish Arora, B.A.I. Computer Engineering, 16338462**

A distributed Network File System (NFS) implementation with:

1. Distributed transparent file access

2. Directory service

3. Locking service

4. Client-side caching

Implemented in Python using sockets.


## Distributed Transparent File Access
The system operates according to the Andrew File System (AFS) model. The Read and write operations on an open file are directed only to the locally cached copy. 
When a modified file is closed, the changed portions are copied back to the file server. Cache consistency is maintained by callback mechanism. 
It can support multiple connected clients, and multiple file servers. It deals exclusively with .txt files.

### Directory Server
The Directory Server is responsible for holding the name of the files and the respective file server holding that file in a list is called directory identifier. 
It also holds the list of file servers and the clients that are connected. Every time a new file server comes online, the directory identifier is updated with
the list of file names in that file server.

### File server
File server holds the file on its server. Whenever a client requests to read or write a file, the file server sends back the copy of the file to the client 
and if the request is for writing a file, it waits for the client to send back the updated version of the file and saves the updated version in the original
file.

### Client Library
All file accesses are made through a client library called *clientThread.py*. This library is a thread which runs every time a client connects to the 
directory server. By using the library, details of the distributed file-system are hidden from the user.

Clients can:

1. View the list of files with directory server by a writing 'SHOW FILES' command on the terminal.

2. Read a file by writing 'READ FILE' followed by the name of the file.

3. Write a file by writing 'WRITE FILE' followed by the name of the file.

4. Create a file by writing 'CREATE FILE' followed by the name of the file.

If the client has request to READ a file, clientProxy ask for the name of the file, request the directory server to send the name of the file server holding 
that particular file. Then the client proxy contacts the file server to send the copy of the file. Once the file server sends the copy of the file, the client 
proxy writes the content of the file in the local directory of the client.

If the client has request to WRITE a file, clientProxy ask for the name of the file, contacts the file server holding the file, receive the content of the file 
and write it to the local directory of the client. Once the client has made the changes, clientProxy get the updated version of the file and sends it back to the 
file server holding the file to save the updated version of the file in the original file.

If the client has request to CREATE  a file, clientProxy ask the directory server to get a file server which is having the minimum load on it. When the 
directory server sends the name of the file server, clientProxy contacts the file server for a file creation.

Every time the client wants to access a file, the file opens in notepad.

## Locking Service
When the directory server comes on-line, it create the object of the lock. Every time a client request for a file write, lock server checks the queue of 
clients requesting to access the file. If queue is empty, the client is granted the access to write the file, if the queue is not empty, the client is 
added to the queue and is requested to wait until all the clients who tried to access the file before that client are given the access to write into the file. 
It allow the client to have exclusive access to the file.

## Client Cache
The client cache is created for every client who connects to the directory server. When a client request to READ or WRITE a file, the clientProxy first checks the 
local cache. If the file is present in the cache, clientProxy opens the file for the client from the cache. If not, clientProxy contacts the file server for the file 
and once the file is received back, clientProxy add the file to the cache using Least Recently Used algorithm to evict the other file from the cache and add the 
requested file to the cache. The client Write something to the file, the clientProxy waits for the client to close the file. Once the file is closed, clientProxy sends 
the updated version to the file server and checks with all the other clientProxy's for the file in their cache. If the file is present in the cache of the there 
clientProxys, it ask them to contact the file server and get the updated version of the file in their respective cache.

## Dependencies

* Python 3.6.2

## Additional Notes
While working on distributed file server, I took a completely wrong approach while implementing directory server, so I started with directory server all over again using file server from the first try in a new repository. While pushing the changes on bitbucket (username - aroraaa), I was encountering problems with merging my new code with the old one, so I forced my recent commits from new repository to my bitbucket account because of which I lost all my previous commits (around 10 in total). 
While evaluation, if you encounter fewer commits on my distributed file system project, please consider the above explanation. 