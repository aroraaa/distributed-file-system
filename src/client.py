import socket
import os

# from pdb import set_trace; set_trace()

BUF_SIZE = 4096
DEFAULT_PORT = 45678


class Client:

    def __init__(self, client_name):
        self.client_socket = None
        self.local_directory_path = "..\Local Directory\%s" % client_name
        self.create_client()

    def create_client(self):
        try:
            self.check_if_directory_exist()
            self.client_socket = socket.socket()
            host = socket.gethostname()
            port = DEFAULT_PORT
            self.client_socket.connect((host, port))
            self.authenticate_to_directory_server()
            print("Welcome to Directory Server\n")
            self.message_exchanger()

        except Exception as e:
            print("Error occurred while connecting client\n")
            print(e.message)

    def authenticate_to_directory_server(self):
        try:
            message = 'Client wants to connect to Directory Server\n'
            self.client_socket.sendall(message.encode())
        except Exception as e:
            print("Error occurred while authenticating client to directory server")
            print(e)

    def message_exchanger(self):
        while 1:
            self.receive_message_from_server()
            self.send_message_to_server()

    def send_message_to_server(self):
        message = input()
        self.client_socket.sendall(message.encode())

    def receive_message_from_server(self):
        try:
            message = self.client_socket.recv(BUF_SIZE)
            if message.decode() == 'Please send local directory path':
                self.client_socket.sendall(self.local_directory_path.encode())
            else:
                print('SERVER: %s' % message.decode())
        except Exception as e:
            print("Error occurred while receiving message from server")
            print(e)

    def client_name(self):
        return self.client_name

    def check_if_directory_exist(self):
        try:
            if not os.path.isdir(self.local_directory_path):
                os.makedirs(self.local_directory_path)
                print("Directory created successfully")
            else:
                print("Directory already exist")
        except Exception as e:
            print("Error occurred while checking the client directory")
            print(e)


if __name__ == '__main__':
    client = Client('Client 1')
