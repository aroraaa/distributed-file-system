class Lock:

    def __init__(self):
        self.list_of_files = list()
        self.list_of_queues = list()

    def take_semaphore(self, file_name, socket):
        try:
            if file_name not in self.list_of_files:
                self.list_of_files.append(file_name)
                self.list_of_queues.append(list())
            index = self.list_of_files.index(file_name)
            if socket not in self.list_of_queues[index]:
                self.list_of_queues[index].append(socket)
            if self.list_of_queues[index].index(socket) == 0:
                print("Enabling lock for %s" % file_name)
                print(self.list_of_queues[index])
                return "Lock Enabled"
            else:
                print("Lock for %s is taken.\nPlease Wait" % file_name)
                return "Lock is taken"

        except Exception as e:
            print("Error occurred in take semaphore")
            print(e)

    def release_semaphore(self, file_name):
        try:
            index = self.list_of_files.index(file_name)
            self.list_of_queues[index].pop(0)
            print("Lock disabled for %s" % file_name)
            print(self.list_of_queues[index])
            return 'Lock disabled'
        except Exception as e:
            print("Error occurred in release semaphore")
            print(e)
