from threading import Thread

DEFAULT_PORT = 45678
BUF_SIZE = 4096


class FileServerProxy(Thread):
    def __init__(self, socket):
        Thread.__init__(self)
        self.socket = socket

    def socket(self):
        return self.socket
