import os

CACHE_SIZE = 3
LEAST_RECENTLY_USED_INDEX = 0


class Cache:
    def __init__(self, local_directory_path):
        self.local_directory_path = local_directory_path
        self.cache_list = list()
        self.update_cache_list()

    def check_file(self, file_name):
        return self.least_recently_used(file_name)

    def least_recently_used(self, file_name):
        if file_name in self.cache_list:
            self.rearrange_cache_list(file_name)
            response = "Cache Hit"
            print(response)
            return response
        else:
            if self.cache_list:
                response = self.get_cache_full()
                if response == 'Full':
                    self.eviction_from_cache()

            self.add_to_cache(file_name)
            response = 'Cache Miss'
            print(response)
            return response

    def rearrange_cache_list(self, file_name):
        index = self.cache_list.index(file_name)
        self.cache_list.pop(index)
        self.cache_list.append(file_name)
        print(self.cache_list)

    def get_cache_full(self):
        if len(self.cache_list) == CACHE_SIZE:
            return "Full"
        else:
            return "Not Full"

    def delete_file_from_local_directory(self, file_name):
        file_path = "%s\%s" % (self.local_directory_path, file_name)
        print("Deleting %s from cache" % file_name)
        os.remove(file_path)

    def update_cache_list(self):
        try:
            self.cache_list = os.listdir(self.local_directory_path)
        except Exception as e:
            print("Error occurred while updating the list of files")
            print(e)

    def eviction_from_cache(self):
        file_name_removing_from_cache = self.cache_list[LEAST_RECENTLY_USED_INDEX]
        self.delete_file_from_local_directory(file_name_removing_from_cache)
        self.cache_list.pop(LEAST_RECENTLY_USED_INDEX)

    def add_to_cache(self, file_name):
        self.cache_list.append(file_name)
        print(self.cache_list)

    def get_cache_list(self):
        return self.cache_list
