import socket
import os

DEFAULT_PORT = 45678
BUF_SIZE = 4096


class FileServer:
    def __init__(self, file_server_name):
        self.file_server_path = "..\Directory\%s" % file_server_name
        self.list_of_files = list()
        self.check_if_directory_exist()
        self.file_server_socket = None
        self.create_file_server()

    def create_file_server(self):
        try:
            self.file_server_socket = socket.socket()
            host = socket.gethostname()
            port = DEFAULT_PORT
            self.file_server_socket.connect((host, port))
            self.authenticate_to_directory_server()
            print("Welcome to Directory Server\n")
            self.message_exchanger()

        except Exception as e:
            print("Error occurred while connecting file server\n")
            print(e)

    def authenticate_to_directory_server(self):
        try:
            message = 'File Server wants to connect to Directory Server'
            self.file_server_socket.sendall(message.encode())
        except Exception as e:
            print("Error occurred while authenticating to server")
            print(e)

    def message_exchanger(self):
        while 1:
            message = self.receive_message_from_server()
            self.send_message_to_server(message)

    def receive_message_from_server(self):
        try:
            message = self.file_server_socket.recv(BUF_SIZE)
            message = message.decode()
            print('CLIENT: %s' % message)
            return message
        except Exception as e:
            print("Error occurred while receiving message from client")
            print(e)

    def send_message_to_server(self, message):
        try:
            response = ''
            if message == 'FILE LIST':
                response = ' '.join(self.list_of_files)
                if len(response) == 0:
                    response = 'List is empty'

            elif message == 'UPDATE THE FILE':
                self.file_server_socket.sendall("Please send the file name".encode())
                file_name = self.file_server_socket.recv(BUF_SIZE).decode()
                self.file_server_socket.sendall("Please send the file content".encode())
                file_content = self.file_server_socket.recv(BUF_SIZE).decode()
                response = self.update_file(file_name, file_content)

            elif message == 'READ THE FILE':
                self.file_server_socket.sendall("Please send the file name".encode())
                file_name = self.file_server_socket.recv(BUF_SIZE).decode()
                response = self.get_read_file(file_name)
                if len(response) == 0:
                    response = "File is empty"

            elif message == 'CREATE FILE':
                self.file_server_socket.sendall("Please send the file name".encode())
                file_name = self.file_server_socket.recv(BUF_SIZE).decode()
                response = self.create_file(file_name)
                if len(response) == 0:
                    response = "Empty file created"
                self.update_list_of_files()

            print(response)
            self.file_server_socket.sendall(response.encode())

        except Exception as e:
            print("Error occurred while sending message to client")
            print(e)

    def check_if_directory_exist(self):
        try:
            if not os.path.isdir(self.file_server_path):
                os.makedirs(self.file_server_path)
                print("File server directory created successfully\n")
            else:
                print("File server directory already exist\n")

            self.update_list_of_files()
        except Exception as e:
            print("Error occurred while checking file server directory")
            print(e)

    def get_read_file(self, file_name):
        try:
            file_path = "%s\%s" % (self.file_server_path, file_name)
            file = open(file_path, "r")
            content = file.read()
            return content
        except Exception as e:
            print("Error occurred in get read file")
            print(e)

    def update_file(self, file_name, file_content):
        try:
            file_path = "%s\%s" % (self.file_server_path, file_name)
            file = open(file_path, "w")
            file.write(file_content)
            response = 'File update successfully'
            return response
        except Exception as e:
            print("Error occurred in update file")
            print(e)

    def create_file(self, file_name):
        try:
            file_path = "%s\%s" % (self.file_server_path, file_name)
            file = open(file_path, "w+")
            content = file.read()
            return content
        except Exception as e:
            print("Error occurred in create file")
            print(e)

    def update_list_of_files(self):
        try:
            self.list_of_files = os.listdir(self.file_server_path)
        except Exception as e:
            print("Error occurred while updating the list of files")
            print(e)

    def list_of_file(self):
        return self.list_of_files


if __name__ == '__main__':
    # file_server_1 = FileServer('File Server 1')
    # file_server_2 = FileServer('File Server 2')
    file_server_3 = FileServer('File Server 3')
    # file_server_3 = FileServer('File Server 4')
