import socket
import os
from src.clientProxy import ClientProxy
from src.fileServerProxy import FileServerProxy
from src.lockService import Lock

DEFAULT_PORT = 45678
DIRECTORY_PATH = "..\Directory"
BUF_SIZE = 4096


class DirectoryServer:

    def __init__(self):
        self.directory_identifiers = list()
        self.file_server_sockets = list()
        self.client_proxies = list()
        self.listenForConnection = False
        self.server_socket = None
        self.check_if_directory_exist()
        self.lock_service = self.enable_lock_service()
        self.create_server()

    def create_server(self):
        try:
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            port = DEFAULT_PORT
            host = socket.gethostname()
            self.server_socket.bind((host, port))
            print("Directory server started at " + str(port) + "\n")
            self.listenForConnection = True
            self.accept_client_connection()

        except Exception as e:
            print("Error occurred while connecting to directory server\n")
            print(e)

    def accept_client_connection(self):
        try:
            self.server_socket.listen(5)
            while self.listenForConnection:
                socket_connection, socket_address = self.server_socket.accept()
                listen = self.listen_for_authentication(socket_connection)
                if listen != 'File Server':
                    print("Client connected\n")
                    self.run_client_thread(socket_connection)
                else:
                    print("File server connected")
                    self.run_file_server_thread(socket_connection)

        except Exception as e:
            print("Error occurred while accepting a connection\n")
            print(e)
            self.server_socket.close()

    def run_client_thread(self, socket):
        try:
            client_proxies = ClientProxy(socket, self.directory_identifiers, self.file_server_sockets, self.lock_service, self.client_proxies)
            self.client_proxies.append(client_proxies)
            client_proxies.start()
        except Exception as e:
            print("Error occurred while running the client thread")
            print(e)

    def run_file_server_thread(self, socket):
        try:
            FileServerProxy(socket).start()
            self.file_server_sockets.append(socket)
            self.update_directory_identifiers(socket)
        except Exception as e:
            print("Error occurred while running the file server thread")
            print(e)

    def check_if_directory_exist(self):
        try:
            if not os.path.isdir(DIRECTORY_PATH):
                os.makedirs(DIRECTORY_PATH)
                print("Directory created successfully")
            else:
                print("Directory already exist")

        except Exception as e:
            print("Error occurred while checking the directory")
            print(e)

    def listen_for_authentication(self, socket):
        try:
            message = socket.recv(BUF_SIZE)
            message = message.decode()
            if message == 'File Server wants to connect to Directory Server':
                return 'File Server'
        except Exception as e:
            print("Error occurred while listening for authentication")
            print(e)

    def update_directory_identifiers(self, socket):
        try:
            self.directory_identifiers.append(list())
            last_index = len(self.directory_identifiers) - 1
            list_of_files = self.get_list_of_files_from_file_server(socket)
            self.directory_identifiers[last_index] = list_of_files
            print(self.directory_identifiers)

        except Exception as e:
            print(e)

    def get_list_of_files_from_file_server(self, socket):
        try:
            message = 'FILE LIST'
            socket.sendall(message.encode())
            message = socket.recv(BUF_SIZE)
            message = message.decode()
            if message == 'List is empty':
                file_list = list()
            else:
                file_list = message.split()
            return file_list
        except Exception as e:
            print("Error occurred while getting the list of files from file server")
            print(e)

    def get_directory_identifiers(self):
        return self.directory_identifiers

    def enable_lock_service(self):
        lock = Lock()
        return lock


if __name__ == '__main__':
    directory_server = DirectoryServer()
