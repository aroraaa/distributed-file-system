from threading import Thread
import subprocess as sp
import time
from src.clinetCache import Cache

BUF_SIZE = 4096


class ClientProxy(Thread):

    def __init__(self, socket, directory_identifiers, file_servers, lock_service, client_threads):
        Thread.__init__(self)
        self.list_of_commands = ['SHOW FILES', 'READ FILE', 'WRITE FILE', 'CREATE FILE']
        self.socket = socket
        self.file_server_sockets = file_servers
        self.directory_identifiers = directory_identifiers
        self.lock_service = lock_service
        self.client_threads = client_threads
        self.local_directory_path = self.get_local_directory_path()
        self.client_cache = Cache(self.local_directory_path)

    def run(self):
        while 1:
            message = self.receive_message_from_client()
            self.send_message_to_client(message)

    def receive_message_from_client(self):
        message = self.socket.recv(BUF_SIZE).decode()
        print('CLIENT: %s' % message)
        return message

    def send_message_to_client(self, message):
        try:
            response = ''
            if message in self.list_of_commands:

                if message == 'SHOW FILES':
                    response = self.response_to_show_file()

                elif message == 'READ FILE':
                    file_name = self.get_file_name()
                    cache_response = self.check_cache_for_file(file_name)
                    if cache_response == 'Cache Hit':
                        self.open_readonly_file_from_cache(file_name)
                        response = 'Opened file locally'
                    else:
                        file_server_socket = self.get_file_server_socket_holding_the_file(file_name)
                        if file_server_socket is None:
                            response = 'File does not exists'
                        else:
                            file_content = self.get_file_from_file_server(file_server_socket, file_name)
                            self.open_readonly_file_from_file_server(file_name, file_content)
                            response = 'Opened file from a remote server'

                elif message == 'WRITE FILE':
                    file_name = self.get_file_name()
                    file_server_socket = self.get_file_server_socket_holding_the_file(file_name)
                    if file_server_socket is None:
                        response = 'File does not exists'
                    else:
                        lock_status = self.lock_service.take_semaphore(file_name, self.socket)
                        if lock_status == 'Lock is taken':
                            while lock_status == 'Lock is taken':
                                time.sleep(5)
                                print("Lock is taken, waiting for its release")
                                lock_status = self.lock_service.take_semaphore(file_name, self.socket)
                        cache_response = self.check_cache_for_file(file_name)
                        if cache_response == 'Cache Hit':
                            file_content = self.open_write_file_from_cache(file_name)
                        else:
                            file_content = self.get_file_from_file_server(file_server_socket, file_name)
                            file_content = self.open_write_file_from_file_server(file_name, file_content)
                        response = self.send_file_to_server(file_server_socket, file_name, file_content)
                        if response == 'Changes saved':
                            lock_status = self.lock_service.release_semaphore(file_name)
                            self.callback(file_name, file_server_socket)

                elif message == 'CREATE FILE':
                    file_name = self.get_file_name()
                    self.check_cache_for_file(file_name)
                    file_server_socket = self.get_file_server_for_file_creation(file_name)
                    file_content = self.request_file_server_for_file_creation(file_server_socket, file_name)
                    file_content = self.open_write_file_from_file_server(file_name, file_content)
                    response = self.send_file_to_server(file_server_socket, file_name, file_content)
            else:
                response = 'UNKNOWN REQUEST'

            self.socket.sendall(response.encode())
            print(response)

        except Exception as e:
            print("Error occurred while sending message to client")
            print(e)

    def response_to_show_file(self):
        try:
            response = ''
            for i in range(len(self.directory_identifiers)):
                response = response + ', '.join(self.directory_identifiers[i]) + ' , '
            if response == '':
                response = 'Directory does not have any files'
            return response
        except Exception as e:
            print("Error occurred while responding to show files")
            print(e)

    def get_file_name(self):
        try:
            message = 'Please enter the file name'
            self.socket.sendall(message.encode())
            response = self.socket.recv(BUF_SIZE).decode()
            return response
        except Exception as e:
            print("Error occurred while getting file name")
            print(e)

    def get_file_server_socket_holding_the_file(self, file_name):
        try:
            for i in range(len(self.directory_identifiers)):
                if file_name in self.directory_identifiers[i]:
                    print("File found with server %s" % (i+1))
                    socket = self.file_server_sockets[i]
                    return socket
        except Exception as e:
            print("Error occurred while getting file server holding the files")
            print(e)

    def get_file_from_file_server(self, socket, file_name):
        try:
            socket.sendall("READ THE FILE".encode())
            socket.recv(BUF_SIZE).decode()
            socket.sendall(file_name.encode())
            message = socket.recv(BUF_SIZE).decode()
            if message == "File is empty":
                message = ''
            return message
        except Exception as e:
            print("Error occurred while getting file from file server")
            print(e)

    def send_file_to_server(self, socket, file_name, file_content):
        try:
            socket.sendall("UPDATE THE FILE".encode())
            socket.recv(BUF_SIZE).decode()
            socket.sendall(file_name.encode())
            socket.recv(BUF_SIZE).decode()
            socket.sendall(file_content.encode())
            response = socket.recv(BUF_SIZE).decode()
            if response == "File update successfully":
                response = 'Changes saved'
            return response
        except Exception as e:
            print("Error occurred while sending file to server")
            print(e)

    def request_file_server_for_file_creation(self, socket, file_name):
        try:
            socket.sendall("CREATE FILE".encode())
            socket.recv(BUF_SIZE).decode()
            socket.sendall(file_name.encode())
            response = socket.recv(BUF_SIZE).decode()
            if response == 'Empty file created':
                response = ''
            return response
        except Exception as e:
            print("Error occurred in request file server for file creation")
            print(e)

    def check_cache_for_file(self, file_name):
        response = self.client_cache.check_file(file_name)
        return response

    def get_local_directory_path(self):
        try:
            message = 'Please send local directory path'
            self.socket.sendall(message.encode())
            message = self.socket.recv(BUF_SIZE).decode()
            return message
        except Exception as e:
            print("Error occurred in get local directory path")
            print(e)

    def open_readonly_file_from_cache(self, file_name):
        try:
            file_path = "%s\%s" % (self.local_directory_path, file_name)
            sp.Popen(["notepad.exe", file_path])
        except Exception as e:
            print("Error occurred while opening file in notepad for reading")
            print(e)

    def open_readonly_file_from_file_server(self, file_name, file_content):
        try:
            file_path = "%s\%s" % (self.local_directory_path, file_name)
            file = open(file_path, "w+")
            file.write(file_content)
            file.close()
            sp.Popen(["notepad.exe", file_path])
        except Exception as e:
            print("Error occurred while opening file in notepad for reading")
            print(e)

    def open_write_file_from_file_server(self, file_name, file_content):
        try:
            file_path = "%s\%s" % (self.local_directory_path, file_name)
            file = open(file_path, "w+")
            file.write(file_content)
            file.close()
            self.socket.sendall("Send DONE when you are finished editing the file".encode())
            sp.Popen(["notepad.exe", file_path]).wait()
            message = self.socket.recv(BUF_SIZE).decode()
            if message == "DONE":
                file = open(file_path, "r")
                file_content = file.read()
                return file_content
        except Exception as e:
            print("Error occurred while opening file in notepad for writing")
            print(e)

    def open_write_file_from_cache(self, file_name):
        try:
            file_path = "%s\%s" % (self.local_directory_path, file_name)
            self.socket.sendall("Send DONE when you are finished editing the file".encode())
            sp.Popen(["notepad.exe", file_path]).wait()
            message = self.socket.recv(BUF_SIZE).decode()
            if message == "DONE":
                file = open(file_path, "r")
                file_content = file.read()
                return file_content

        except Exception as e:
            print("Error occurred while opening file in notepad for writing")
            print(e)

    def get_file_server_for_file_creation(self, file_name):
        try:
            list_of_min_no_of_files = min(self.directory_identifiers, key=len)
            for i in range(len(self.directory_identifiers)):
                if list_of_min_no_of_files == self.directory_identifiers[i]:
                    socket = self.file_server_sockets[i]
                    self.update_directory_identifiers(i, file_name)
                    return socket
        except Exception as e:
            print("Error occurred in get file server for file creation")
            print(e)

    def get_updated_content_of_file(self, file_name, file_content):
        try:
            file_path = "%s\%s" % (self.local_directory_path, file_name)
            file = open(file_path, "w+")
            file.write(file_content)
            file.close()
        except Exception as e:
            print("Error occurred while updating the content of file")
            print(e)

    def callback(self, file_name, socket):
        try:
            for i in range(len(self.client_threads)):
                client_proxy = self.client_threads[i]
                cache_of_client = client_proxy.get_cache()
                cache_list_of_client = cache_of_client.get_cache_list()
                if file_name in cache_list_of_client:
                    file_content = client_proxy.get_file_from_file_server(socket, file_name)
                    client_proxy.get_updated_content_of_file(file_name, file_content)
        except Exception as e:
            print("Error occurred in callback function")
            print(e)

    def get_cache(self):
        return self.client_cache

    def update_directory_identifiers(self, i, file_name):
        self.directory_identifiers[i].append(file_name)
        print(self.directory_identifiers)

